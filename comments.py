from bda_connection import get_database
from bson import ObjectId

dbname = get_database()
collection_name = dbname["users"]
users = collection_name

class Comment:


    def agregar_comentario(self, name, url, user_id, article_id):
        new_comment = {
            'name': name,
            'url': url,
            'user_id': ObjectId(user_id),
            'article_id': ObjectId(article_id)
        }
        resultado = self.collection.insert_one(new_comment)
        return resultado.inserted_id

    def eliminar_comentario(self, comment_id):
        resultado = self.collection.delete_one({'_id': ObjectId(comment_id)})
        return resultado.deleted_count > 0

    def modificar_comentario(self, comment_id, new_name, new_url):
        resultado = self.collection.update_one(
            {'_id': ObjectId(comment_id)},
            {'$set': {'name': new_name, 'url': new_url}}
        )
        return resultado.modified_count > 0

    def buscar_comentario(self, comment_id):
        return self.collection.find_one({'_id': ObjectId(comment_id)})

    def obtener_comentarios(self, article_id):
        return list(self.collection.find({'article_id': ObjectId(article_id)}))
