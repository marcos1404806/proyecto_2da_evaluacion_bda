from bda_connection import get_database
from users import Users
from articles import Article
from comments import Comment


def main():

    while True:
        print("\n----- Menú Principal -----")
        print("1. Gestionar Usuarios")
        print("2. Gestionar Artículos")
        print("3. Ver Artículos")

        print("6. Salir")

        op_menup = input("Seleccione una opción: ")

        if op_menup == '1':
            while True:
                print("\n----- Menú Usuarios -----")
                print("1. Agregar Usuario")
                print("2. Eliminar Usuario")
                print("3. Modificar Usuario")
                print("4. Volver al Menú Principal")

                op_menus = input("Seleccione una opción: ")

                if op_menus == '1':
                    name = input("Ingrese el nombre del usuario: ")
                    email = input("Ingrese el correo electrónico del usuario: ")
                    Users.agregar_usuario(name, email)
                    print("Usuario agregado exitosamente.")
                
                elif op_menus == '2':
                    user_id = input("Ingrese el ID del usuario a eliminar: ")
                    if Users.eliminar_usuario(user_id):
                        print("Usuario eliminado exitosamente.")
                    else:
                        print("No se pudo eliminar el usuario.")

                elif op_menus == '3':
                    user_id = input("Ingrese el ID del usuario a modificar: ")
                    new_name = input("Ingrese el nuevo nombre del usuario: ")
                    new_email = input("Ingrese el nuevo correo electrónico del usuario: ")
                    if Users.modificar_usuario(user_id, new_name, new_email):
                        print("Usuario modificado exitosamente.")
                    else:
                        print("No se pudo modificar el usuario.")
                
                elif op_menus == '4':
                    break
                
                else:
                    print("Opción no válida. Intente de nuevo.")

        elif op_menup == '2':
            while True:
                print("\n----- Menú Artículos -----")
                print("1. Agregar Artículo")
                print("2. Eliminar Artículo")
                print("3. Modificar Artículo")
                print("4. Volver al Menú Principal")

                op_menuart = input("Seleccione una opción: ")

                if op_menuart == '1':
                    title = input("Ingrese el título del artículo: ")
                    date = input("Ingrese la fecha del artículo: ")
                    text = input("Ingrese el texto del artículo: ")
                    Article.add_article(title, date, text)
                    print("Artículo agregado exitosamente.")

                elif op_menuart == '2':
                    article_id = input("Ingrese el ID del artículo a eliminar: ")
                    if Article.eliminar_articulo(article_id):
                        print("Artículo eliminado exitosamente.")
                    else:
                        print("No se pudo eliminar el artículo.")

                
                elif op_menuart == '3':
                    article_id = input("Ingrese el ID del artículo a modificar: ")
                    new_title = input("Ingrese el nuevo título del artículo: ")
                    new_date = input("Ingrese la nueva fecha del artículo: ")
                    new_text = input("Ingrese el nuevo texto del artículo: ")
                    if Article.modificar_articulo(article_id, new_title, new_date, new_text):
                        print("Artículo modificado exitosamente.")
                    else:
                        print("No se pudo modificar el artículo.")

                elif op_menuart == '4':
                    break
                
                else:
                    print("Opción no válida. Intente de nuevo.")

        elif op_menup == '3':
            articulos = op_menuart(Article)
            seleccion = input("Seleccione un ID de artículo: ")

            try:
                seleccion = int(seleccion)
                if 1 <= seleccion <= len(articulos):
                    seleccionado = articulos[seleccion - 1]
                    article_id = seleccionado['_id']
                    agregar_comentario(Comment, Users, article_id)
                else:
                    print("ID de artículo no válido.")
            except ValueError:
                print("Ingrese un número válido.")
        
        elif op_menup == '6':
            print("Saliendo...")
            break

        else:
            print("Opción no válida. Intente de nuevo.")

if __name__ == "__main__":
    main()

       
