from bda_connection import get_database
from bson import ObjectId

dbname = get_database()
collection_name = dbname["articles"]
articles = collection_name

articles.insert_one({"title": "Artículo", "date": "00/00/0000", "text": "Lorem ipsum dices tú"})

class Article:

    def add_article(self, title, date, text):
        new_article = {
            'title': title,
            'date': date,
            'text': text
        }
        res = self.collection.insert_one(new_article)
        return res.inserted_id

    def eliminar_articulo(self, article_id):
        res = self.collection.delete_one({'_id': ObjectId(article_id)})
        return res.deleted_count > 0

    def modificar_articulo(self, article_id, new_title, new_date, new_text):
        res = self.collection.update_one(
            {'_id': ObjectId(article_id)},
            {'$set': {'title': new_title, 'date': new_date, 'text': new_text}}
        )
        return res.modified_count > 0

    def buscar_articulo(self, article_id):
        return self.collection.find_one({'_id': ObjectId(article_id)})
    
    def obtener_todos_los_articulos(self):
        return list(self.collection.find({}, {'title': 1}))
