from bda_connection import get_database
from bson import ObjectId

dbname = get_database()
collection_name = dbname["users"]
users = collection_name

users.insert_one({"username": "jose", "email": "a@asd.com"})

class Users:

    '''
    def __init__(self, conexion):
        self.conexion = conexion
        self.collection = self.conexion.db['users'] '''

    def agregar_usuario(self, name, email):
        new_user = {
            'name': name,
            'email': email
        }
        res = self.collection.insert_one(new_user)
        return res.inserted_id

    def eliminar_usuario(self, user_id):
        res = self.collection.delete_one({'_id': ObjectId(user_id)})
        return res.deleted_count > 0

    def modificar_usuario(self, user_id, new_name, new_email):
        res = self.collection.update_one(
            {'_id': ObjectId(user_id)},
            {'$set': {'name': new_name, 'email': new_email}}
        )
        return res.modified_count > 0

    def buscar_usuario(self, user_id):
        return self.collection.find_one({'_id': ObjectId(user_id)})


